<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Origin extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->home();
    }

    public function page1() {
        $this->presentation();
    }

    public function home() {
        //$this->load->view('theme1/header');
        //$this->load->view('theme1/navbar');
        //$this->load->view('theme1/home');
        //$this->load->view('theme1/footer');

        $this->load->view('test/test2');
    }

    public function presentation() {
        $data = array();
        $data['pseudo'] = 'Olivier';
        $data['email'] = 'contact@loliveserious.com';
        $data['en_ligne'] = true;

        $this->load->view('test/test', $data);
    }

}
